﻿namespace TXI_Laba__5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.aButt1 = new System.Windows.Forms.Button();
            this.flowersBox = new System.Windows.Forms.ListBox();
            this.textIn2 = new System.Windows.Forms.TextBox();
            this.textIn = new System.Windows.Forms.TextBox();
            this.stats1 = new System.Windows.Forms.Label();
            this.stats2 = new System.Windows.Forms.Label();
            this.addFlower = new System.Windows.Forms.Button();
            this.addPackage = new System.Windows.Forms.Button();
            this.informbar = new System.Windows.Forms.Label();
            this.packmateriallogo = new System.Windows.Forms.Label();
            this.packagepricelogo = new System.Windows.Forms.Label();
            this.informationmaterial = new System.Windows.Forms.Label();
            this.flowerlistlogo = new System.Windows.Forms.Label();
            this.flowernamelogo = new System.Windows.Forms.Label();
            this.flowerpricelogo = new System.Windows.Forms.Label();
            this.flowname = new System.Windows.Forms.Label();
            this.flowprice = new System.Windows.Forms.Label();
            this.typeOfPlant1 = new System.Windows.Forms.RadioButton();
            this.typeOfPlant2 = new System.Windows.Forms.RadioButton();
            this.typeOfPlantlogo = new System.Windows.Forms.RadioButton();
            this.textIn3 = new System.Windows.Forms.TextBox();
            this.stats3 = new System.Windows.Forms.Label();
            this.stats4 = new System.Windows.Forms.Label();
            this.textIn4 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.typeOf = new System.Windows.Forms.GroupBox();
            this.typeOfFlow4 = new System.Windows.Forms.RadioButton();
            this.typeOfFlow3 = new System.Windows.Forms.RadioButton();
            this.typeOfFlow2 = new System.Windows.Forms.RadioButton();
            this.typeOfFlow1 = new System.Windows.Forms.RadioButton();
            this.RemoveFlowerButton = new System.Windows.Forms.Button();
            this.RemoveAllButton = new System.Windows.Forms.Button();
            this.typeOf.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Cyan;
            this.button1.Location = new System.Drawing.Point(575, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.UseWaitCursor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // aButt1
            // 
            this.aButt1.Enabled = false;
            this.aButt1.Location = new System.Drawing.Point(167, 342);
            this.aButt1.Name = "aButt1";
            this.aButt1.Size = new System.Drawing.Size(75, 23);
            this.aButt1.TabIndex = 2;
            this.aButt1.Text = "Собрать";
            this.aButt1.UseVisualStyleBackColor = true;
            this.aButt1.UseWaitCursor = true;
            this.aButt1.Click += new System.EventHandler(this.aButt1_Click);
            // 
            // flowersBox
            // 
            this.flowersBox.FormattingEnabled = true;
            this.flowersBox.Location = new System.Drawing.Point(12, 93);
            this.flowersBox.Name = "flowersBox";
            this.flowersBox.Size = new System.Drawing.Size(120, 95);
            this.flowersBox.TabIndex = 6;
            this.flowersBox.UseWaitCursor = true;
            this.flowersBox.SelectedIndexChanged += new System.EventHandler(this.flowersBox_SelectedIndexChanged);
            // 
            // textIn2
            // 
            this.textIn2.Enabled = false;
            this.textIn2.Location = new System.Drawing.Point(283, 230);
            this.textIn2.Name = "textIn2";
            this.textIn2.Size = new System.Drawing.Size(117, 20);
            this.textIn2.TabIndex = 8;
            this.textIn2.UseWaitCursor = true;
            // 
            // textIn
            // 
            this.textIn.Enabled = false;
            this.textIn.Location = new System.Drawing.Point(283, 191);
            this.textIn.Name = "textIn";
            this.textIn.Size = new System.Drawing.Size(117, 20);
            this.textIn.TabIndex = 9;
            this.textIn.UseWaitCursor = true;
            // 
            // stats1
            // 
            this.stats1.AutoSize = true;
            this.stats1.Location = new System.Drawing.Point(280, 175);
            this.stats1.Name = "stats1";
            this.stats1.Size = new System.Drawing.Size(35, 13);
            this.stats1.TabIndex = 10;
            this.stats1.Text = "stats1";
            this.stats1.UseWaitCursor = true;
            this.stats1.Visible = false;
            // 
            // stats2
            // 
            this.stats2.AutoSize = true;
            this.stats2.Location = new System.Drawing.Point(280, 214);
            this.stats2.Name = "stats2";
            this.stats2.Size = new System.Drawing.Size(35, 13);
            this.stats2.TabIndex = 11;
            this.stats2.Text = "stats2";
            this.stats2.UseWaitCursor = true;
            this.stats2.Visible = false;
            // 
            // addFlower
            // 
            this.addFlower.Location = new System.Drawing.Point(296, 64);
            this.addFlower.Name = "addFlower";
            this.addFlower.Size = new System.Drawing.Size(105, 29);
            this.addFlower.TabIndex = 12;
            this.addFlower.Text = "Добавить цветок";
            this.addFlower.UseVisualStyleBackColor = true;
            this.addFlower.UseWaitCursor = true;
            this.addFlower.Click += new System.EventHandler(this.addFlower_Click);
            // 
            // addPackage
            // 
            this.addPackage.Location = new System.Drawing.Point(296, 22);
            this.addPackage.Name = "addPackage";
            this.addPackage.Size = new System.Drawing.Size(104, 34);
            this.addPackage.TabIndex = 13;
            this.addPackage.Text = "Создать упаковку";
            this.addPackage.UseVisualStyleBackColor = true;
            this.addPackage.UseWaitCursor = true;
            this.addPackage.Click += new System.EventHandler(this.addPackage_Click);
            // 
            // informbar
            // 
            this.informbar.AutoSize = true;
            this.informbar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.informbar.Location = new System.Drawing.Point(15, 318);
            this.informbar.Name = "informbar";
            this.informbar.Size = new System.Drawing.Size(50, 13);
            this.informbar.TabIndex = 14;
            this.informbar.Text = "informbar";
            this.informbar.UseWaitCursor = true;
            this.informbar.Visible = false;
            // 
            // packmateriallogo
            // 
            this.packmateriallogo.AutoSize = true;
            this.packmateriallogo.Location = new System.Drawing.Point(15, 22);
            this.packmateriallogo.Name = "packmateriallogo";
            this.packmateriallogo.Size = new System.Drawing.Size(60, 13);
            this.packmateriallogo.TabIndex = 15;
            this.packmateriallogo.Text = "Материал:";
            this.packmateriallogo.UseWaitCursor = true;
            this.packmateriallogo.Click += new System.EventHandler(this.packmateriallogo_Click);
            // 
            // packagepricelogo
            // 
            this.packagepricelogo.AutoSize = true;
            this.packagepricelogo.Location = new System.Drawing.Point(16, 43);
            this.packagepricelogo.Name = "packagepricelogo";
            this.packagepricelogo.Size = new System.Drawing.Size(89, 13);
            this.packagepricelogo.TabIndex = 16;
            this.packagepricelogo.Text = "Цена упаковки: ";
            this.packagepricelogo.UseWaitCursor = true;
            this.packagepricelogo.Click += new System.EventHandler(this.materialpricelogo_Click);
            // 
            // informationmaterial
            // 
            this.informationmaterial.AutoSize = true;
            this.informationmaterial.Location = new System.Drawing.Point(15, 9);
            this.informationmaterial.Name = "informationmaterial";
            this.informationmaterial.Size = new System.Drawing.Size(138, 13);
            this.informationmaterial.TabIndex = 17;
            this.informationmaterial.Text = "Информация об упаковке";
            this.informationmaterial.UseWaitCursor = true;
            this.informationmaterial.Click += new System.EventHandler(this.label2_Click);
            // 
            // flowerlistlogo
            // 
            this.flowerlistlogo.AutoSize = true;
            this.flowerlistlogo.Location = new System.Drawing.Point(15, 64);
            this.flowerlistlogo.Name = "flowerlistlogo";
            this.flowerlistlogo.Size = new System.Drawing.Size(80, 13);
            this.flowerlistlogo.TabIndex = 18;
            this.flowerlistlogo.Text = "Состав букета";
            this.flowerlistlogo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.flowerlistlogo.UseWaitCursor = true;
            // 
            // flowernamelogo
            // 
            this.flowernamelogo.AutoSize = true;
            this.flowernamelogo.Location = new System.Drawing.Point(157, 93);
            this.flowernamelogo.Name = "flowernamelogo";
            this.flowernamelogo.Size = new System.Drawing.Size(67, 13);
            this.flowernamelogo.TabIndex = 19;
            this.flowernamelogo.Text = "Имя цветка";
            this.flowernamelogo.UseWaitCursor = true;
            // 
            // flowerpricelogo
            // 
            this.flowerpricelogo.AutoSize = true;
            this.flowerpricelogo.Location = new System.Drawing.Point(157, 133);
            this.flowerpricelogo.Name = "flowerpricelogo";
            this.flowerpricelogo.Size = new System.Drawing.Size(97, 13);
            this.flowerpricelogo.TabIndex = 20;
            this.flowerpricelogo.Text = "Цена цветка (руб)";
            this.flowerpricelogo.UseWaitCursor = true;
            // 
            // flowname
            // 
            this.flowname.AutoSize = true;
            this.flowname.Location = new System.Drawing.Point(157, 106);
            this.flowname.Name = "flowname";
            this.flowname.Size = new System.Drawing.Size(35, 13);
            this.flowname.TabIndex = 21;
            this.flowname.Text = "label3";
            this.flowname.UseWaitCursor = true;
            this.flowname.Visible = false;
            // 
            // flowprice
            // 
            this.flowprice.AutoSize = true;
            this.flowprice.Location = new System.Drawing.Point(157, 146);
            this.flowprice.Name = "flowprice";
            this.flowprice.Size = new System.Drawing.Size(35, 13);
            this.flowprice.TabIndex = 22;
            this.flowprice.Text = "label4";
            this.flowprice.UseWaitCursor = true;
            this.flowprice.Visible = false;
            // 
            // typeOfPlant1
            // 
            this.typeOfPlant1.AutoSize = true;
            this.typeOfPlant1.Enabled = false;
            this.typeOfPlant1.Location = new System.Drawing.Point(12, 214);
            this.typeOfPlant1.Name = "typeOfPlant1";
            this.typeOfPlant1.Size = new System.Drawing.Size(62, 17);
            this.typeOfPlant1.TabIndex = 23;
            this.typeOfPlant1.Text = "Цветок";
            this.typeOfPlant1.UseVisualStyleBackColor = true;
            this.typeOfPlant1.UseWaitCursor = true;
            this.typeOfPlant1.CheckedChanged += new System.EventHandler(this.typeOfPlant1_CheckedChanged);
            // 
            // typeOfPlant2
            // 
            this.typeOfPlant2.AutoSize = true;
            this.typeOfPlant2.Enabled = false;
            this.typeOfPlant2.Location = new System.Drawing.Point(12, 237);
            this.typeOfPlant2.Name = "typeOfPlant2";
            this.typeOfPlant2.Size = new System.Drawing.Size(117, 17);
            this.typeOfPlant2.TabIndex = 24;
            this.typeOfPlant2.Text = "Луговое растение";
            this.typeOfPlant2.UseVisualStyleBackColor = true;
            this.typeOfPlant2.UseWaitCursor = true;
            this.typeOfPlant2.CheckedChanged += new System.EventHandler(this.typeOfPlant2_CheckedChanged);
            // 
            // typeOfPlantlogo
            // 
            this.typeOfPlantlogo.AutoSize = true;
            this.typeOfPlantlogo.Enabled = false;
            this.typeOfPlantlogo.Location = new System.Drawing.Point(12, 260);
            this.typeOfPlantlogo.Name = "typeOfPlantlogo";
            this.typeOfPlantlogo.Size = new System.Drawing.Size(93, 17);
            this.typeOfPlantlogo.TabIndex = 25;
            this.typeOfPlantlogo.TabStop = true;
            this.typeOfPlantlogo.Text = "Произвольно";
            this.typeOfPlantlogo.UseVisualStyleBackColor = true;
            this.typeOfPlantlogo.UseWaitCursor = true;
            this.typeOfPlantlogo.CheckedChanged += new System.EventHandler(this.typeOfPlant3_CheckedChanged);
            // 
            // textIn3
            // 
            this.textIn3.Enabled = false;
            this.textIn3.Location = new System.Drawing.Point(283, 270);
            this.textIn3.Name = "textIn3";
            this.textIn3.Size = new System.Drawing.Size(118, 20);
            this.textIn3.TabIndex = 26;
            this.textIn3.UseWaitCursor = true;
            // 
            // stats3
            // 
            this.stats3.AutoSize = true;
            this.stats3.Location = new System.Drawing.Point(281, 254);
            this.stats3.Name = "stats3";
            this.stats3.Size = new System.Drawing.Size(35, 13);
            this.stats3.TabIndex = 27;
            this.stats3.Text = "stats3";
            this.stats3.UseWaitCursor = true;
            this.stats3.Visible = false;
            // 
            // stats4
            // 
            this.stats4.AutoSize = true;
            this.stats4.Location = new System.Drawing.Point(281, 293);
            this.stats4.Name = "stats4";
            this.stats4.Size = new System.Drawing.Size(35, 13);
            this.stats4.TabIndex = 29;
            this.stats4.Text = "stats4";
            this.stats4.UseWaitCursor = true;
            this.stats4.Visible = false;
            // 
            // textIn4
            // 
            this.textIn4.Enabled = false;
            this.textIn4.Location = new System.Drawing.Point(284, 309);
            this.textIn4.Name = "textIn4";
            this.textIn4.Size = new System.Drawing.Size(116, 20);
            this.textIn4.TabIndex = 30;
            this.textIn4.UseWaitCursor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(9, 195);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(120, 88);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип растения";
            this.groupBox1.UseWaitCursor = true;
            // 
            // typeOf
            // 
            this.typeOf.Controls.Add(this.typeOfFlow4);
            this.typeOf.Controls.Add(this.typeOfFlow3);
            this.typeOf.Controls.Add(this.typeOfFlow2);
            this.typeOf.Controls.Add(this.typeOfFlow1);
            this.typeOf.Location = new System.Drawing.Point(149, 195);
            this.typeOf.Name = "typeOf";
            this.typeOf.Size = new System.Drawing.Size(93, 111);
            this.typeOf.TabIndex = 37;
            this.typeOf.TabStop = false;
            this.typeOf.UseWaitCursor = true;
            this.typeOf.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // typeOfFlow4
            // 
            this.typeOfFlow4.AutoSize = true;
            this.typeOfFlow4.Enabled = false;
            this.typeOfFlow4.Location = new System.Drawing.Point(7, 92);
            this.typeOfFlow4.Name = "typeOfFlow4";
            this.typeOfFlow4.Size = new System.Drawing.Size(84, 17);
            this.typeOfFlow4.TabIndex = 3;
            this.typeOfFlow4.TabStop = true;
            this.typeOfFlow4.Text = "typeOfFlow4";
            this.typeOfFlow4.UseVisualStyleBackColor = true;
            this.typeOfFlow4.UseWaitCursor = true;
            this.typeOfFlow4.Visible = false;
            this.typeOfFlow4.CheckedChanged += new System.EventHandler(this.typeOfFlow4_CheckedChanged);
            // 
            // typeOfFlow3
            // 
            this.typeOfFlow3.AutoSize = true;
            this.typeOfFlow3.Enabled = false;
            this.typeOfFlow3.Location = new System.Drawing.Point(7, 68);
            this.typeOfFlow3.Name = "typeOfFlow3";
            this.typeOfFlow3.Size = new System.Drawing.Size(84, 17);
            this.typeOfFlow3.TabIndex = 2;
            this.typeOfFlow3.TabStop = true;
            this.typeOfFlow3.Text = "typeOfFlow3";
            this.typeOfFlow3.UseVisualStyleBackColor = true;
            this.typeOfFlow3.UseWaitCursor = true;
            this.typeOfFlow3.Visible = false;
            this.typeOfFlow3.CheckedChanged += new System.EventHandler(this.typeOfFlow3_CheckedChanged);
            // 
            // typeOfFlow2
            // 
            this.typeOfFlow2.AutoSize = true;
            this.typeOfFlow2.Enabled = false;
            this.typeOfFlow2.Location = new System.Drawing.Point(7, 44);
            this.typeOfFlow2.Name = "typeOfFlow2";
            this.typeOfFlow2.Size = new System.Drawing.Size(84, 17);
            this.typeOfFlow2.TabIndex = 1;
            this.typeOfFlow2.TabStop = true;
            this.typeOfFlow2.Text = "typeOfFlow2";
            this.typeOfFlow2.UseVisualStyleBackColor = true;
            this.typeOfFlow2.UseWaitCursor = true;
            this.typeOfFlow2.Visible = false;
            this.typeOfFlow2.CheckedChanged += new System.EventHandler(this.typeOfFlow2_CheckedChanged);
            // 
            // typeOfFlow1
            // 
            this.typeOfFlow1.AutoSize = true;
            this.typeOfFlow1.Enabled = false;
            this.typeOfFlow1.Location = new System.Drawing.Point(7, 20);
            this.typeOfFlow1.Name = "typeOfFlow1";
            this.typeOfFlow1.Size = new System.Drawing.Size(84, 17);
            this.typeOfFlow1.TabIndex = 0;
            this.typeOfFlow1.TabStop = true;
            this.typeOfFlow1.Text = "typeOfFlow1";
            this.typeOfFlow1.UseVisualStyleBackColor = true;
            this.typeOfFlow1.UseWaitCursor = true;
            this.typeOfFlow1.Visible = false;
            this.typeOfFlow1.CheckedChanged += new System.EventHandler(this.typeOfFlow1_CheckedChanged);
            // 
            // RemoveFlowerButton
            // 
            this.RemoveFlowerButton.Enabled = false;
            this.RemoveFlowerButton.Location = new System.Drawing.Point(296, 101);
            this.RemoveFlowerButton.Name = "RemoveFlowerButton";
            this.RemoveFlowerButton.Size = new System.Drawing.Size(104, 23);
            this.RemoveFlowerButton.TabIndex = 38;
            this.RemoveFlowerButton.Text = "Удалить цветок";
            this.RemoveFlowerButton.UseVisualStyleBackColor = true;
            this.RemoveFlowerButton.UseWaitCursor = true;
            this.RemoveFlowerButton.Click += new System.EventHandler(this.RemoveFlowerButton_Click);
            // 
            // RemoveAllButton
            // 
            this.RemoveAllButton.Enabled = false;
            this.RemoveAllButton.Location = new System.Drawing.Point(296, 133);
            this.RemoveAllButton.Name = "RemoveAllButton";
            this.RemoveAllButton.Size = new System.Drawing.Size(105, 23);
            this.RemoveAllButton.TabIndex = 39;
            this.RemoveAllButton.Text = "Удалить всё";
            this.RemoveAllButton.UseVisualStyleBackColor = true;
            this.RemoveAllButton.UseWaitCursor = true;
            this.RemoveAllButton.Click += new System.EventHandler(this.RemoveAllButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(413, 377);
            this.Controls.Add(this.RemoveAllButton);
            this.Controls.Add(this.RemoveFlowerButton);
            this.Controls.Add(this.textIn4);
            this.Controls.Add(this.stats4);
            this.Controls.Add(this.stats3);
            this.Controls.Add(this.textIn3);
            this.Controls.Add(this.typeOfPlantlogo);
            this.Controls.Add(this.typeOfPlant2);
            this.Controls.Add(this.typeOfPlant1);
            this.Controls.Add(this.flowprice);
            this.Controls.Add(this.flowname);
            this.Controls.Add(this.flowerpricelogo);
            this.Controls.Add(this.flowernamelogo);
            this.Controls.Add(this.flowerlistlogo);
            this.Controls.Add(this.informationmaterial);
            this.Controls.Add(this.packagepricelogo);
            this.Controls.Add(this.packmateriallogo);
            this.Controls.Add(this.informbar);
            this.Controls.Add(this.addPackage);
            this.Controls.Add(this.addFlower);
            this.Controls.Add(this.stats2);
            this.Controls.Add(this.stats1);
            this.Controls.Add(this.textIn);
            this.Controls.Add(this.textIn2);
            this.Controls.Add(this.flowersBox);
            this.Controls.Add(this.aButt1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.typeOf);
            this.Name = "Form1";
            this.Text = "PackageInfomation";
            this.UseWaitCursor = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.typeOf.ResumeLayout(false);
            this.typeOf.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button aButt1;
        private System.Windows.Forms.ListBox flowersBox;
        private System.Windows.Forms.TextBox textIn2;
        private System.Windows.Forms.TextBox textIn;
        private System.Windows.Forms.Label stats1;
        private System.Windows.Forms.Label stats2;
        private System.Windows.Forms.Button addFlower;
        private System.Windows.Forms.Button addPackage;
        private System.Windows.Forms.Label informbar;
        private System.Windows.Forms.Label packmateriallogo;
        private System.Windows.Forms.Label packagepricelogo;
        private System.Windows.Forms.Label informationmaterial;
        private System.Windows.Forms.Label flowerlistlogo;
        private System.Windows.Forms.Label flowernamelogo;
        private System.Windows.Forms.Label flowerpricelogo;
        private System.Windows.Forms.Label flowname;
        private System.Windows.Forms.Label flowprice;
        private System.Windows.Forms.RadioButton typeOfPlant1;
        private System.Windows.Forms.RadioButton typeOfPlant2;
        private System.Windows.Forms.RadioButton typeOfPlantlogo;
        private System.Windows.Forms.TextBox textIn3;
        private System.Windows.Forms.Label stats3;
        private System.Windows.Forms.Label stats4;
        private System.Windows.Forms.TextBox textIn4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox typeOf;
        private System.Windows.Forms.RadioButton typeOfFlow4;
        private System.Windows.Forms.RadioButton typeOfFlow3;
        private System.Windows.Forms.RadioButton typeOfFlow2;
        private System.Windows.Forms.RadioButton typeOfFlow1;
        private System.Windows.Forms.Button RemoveFlowerButton;
        private System.Windows.Forms.Button RemoveAllButton;
    }
}

