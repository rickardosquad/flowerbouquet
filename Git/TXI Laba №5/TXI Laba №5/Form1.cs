﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using logic;
using bouquet;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TXI_Laba__5
{
    public partial class Form1 : Form
    {
        List<Flowers> flowersList = new List<Flowers>();
        WinBouquetMaker winBouquetMaker = new WinBouquetMaker();
        Package package = new Package();
        int buttomNumber;
        int bouquetPrice;
        int countOfFlowers = 0;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
   


        private void aButt1_Click(object sender, EventArgs e)
        {
            if (aButt1.Text == "Собрать")
            {
                bouquetPrice = winBouquetMaker.CountPriceOfBouquet(flowersList, package);
                informbar.Text = ("Букет стоит " + bouquetPrice + " рублей");
            }

            if (aButt1.Text == "Создать")
            {
                informbar.ForeColor = Color.Black;
                if (buttomNumber == 1 && textIn.Text != "" && textIn2.Text != "")
                {
                    if (typeOfPlant1.Checked)
                    {
                        CutFlowers cutflowers = winBouquetMaker.SetCutFlowersParams(textIn.Text, Convert.ToInt32(textIn2.Text), textIn3.Text);

                        if (typeOfFlow1.Checked)
                        {
                            Rose rose = winBouquetMaker.SetRose(cutflowers, textIn4.Text);
                            flowersBox.Items.Add(rose.name);
                            flowersList.Add(rose);
                        }
                        else if (typeOfFlow2.Checked)
                        {
                            Charmomile charmomile = winBouquetMaker.SetCharmomile(cutflowers, Convert.ToInt32(textIn4.Text));
                            flowersBox.Items.Add(charmomile.name);
                            flowersList.Add(charmomile);
                        }
                        else if (typeOfFlow3.Checked)
                        {
                            Peony peony = winBouquetMaker.SetPeony(cutflowers, Convert.ToInt32(textIn4.Text));
                            flowersBox.Items.Add(peony.name);
                            flowersList.Add(peony);
                        }
                        else if (typeOfFlow4.Checked)
                        {
                            flowersList.Add(cutflowers);
                            flowersBox.Items.Add(cutflowers.name);
                        }
                    }
                    if (typeOfPlant2.Checked)
                    {
                        Greens greens = winBouquetMaker.SetGreensParams(textIn.Text, Convert.ToInt32(textIn2.Text), textIn3.Text);
                        flowersList.Add(greens);
                        flowersBox.Items.Add(greens.name);
                        if (typeOfFlow1.Checked)
                        {
                            Fern fern = winBouquetMaker.SetFern(greens, Convert.ToInt32(textIn4.Text));
                            flowersBox.Items.Add(fern.name);
                            flowersList.Add(fern);
                        }
                        else if (typeOfFlow2.Checked)
                        {
                            Bergras bergras = winBouquetMaker.SetBergras(greens, Convert.ToInt32(textIn4.Text));
                            flowersBox.Items.Add(bergras.name);
                            flowersList.Add(bergras);
                        }
                        else if (typeOfFlow3.Checked)
                        {
                            flowersList.Add(greens);
                            flowersBox.Items.Add(greens.name);
                        }
                    }
            
                    if (typeOfPlantlogo.Checked)
                    {
                        Flowers flowers = winBouquetMaker.SetFlower(textIn.Text, Convert.ToInt32(textIn2.Text));
                        flowersList.Add(flowers);
                        flowersBox.Items.Add(flowers.name);
                    }
                    
                    UnableAndUnvisible();
                    informbar.Text = "Цветок добавлен";
                    countOfFlowers++;

                    if (flowersList.Count == 0 || package == null)
                        aButt1.Enabled = false;
                }
                else if(buttomNumber == 0 && textIn.Text != "" && textIn2.Text != "")
                {
                    package = winBouquetMaker.MakePackage(textIn.Text, Convert.ToInt32(textIn2.Text));
                    packmateriallogo.Text = ("Материал: " + package.material);
                    packagepricelogo.Text = ("Цена: " + package.costOfMaterial + " руб");
                    UnableAndUnvisible();
                    informbar.Text = "Упаковка добавлена";
                    if (flowersList.Count == 0 || package.costOfMaterial == 0)
                        aButt1.Enabled = false;
                }
                else
                {
                    informbar.ForeColor = Color.Red;
                    informbar.Text = "Необходимо заполнить все строки!";
                }
            }

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void addFlower_Click(object sender, EventArgs e)
        {
            typeOfPlant1.Enabled = true;
            typeOfPlant2.Enabled = true;
            typeOfPlantlogo.Enabled = true;
            buttomNumber = 1;
            EnableandVisible();
            stats1.Text = "Название цветка";
            stats2.Text = "Цена цветка (руб)";
            informbar.Text = "Добавление цветка";
        }

        private void addPackage_Click(object sender, EventArgs e)
        {
            buttomNumber = 0;
            EnableandVisible();
            stats1.Text = "Материал упаковки";
            stats2.Text = "Цена упаковки (руб)";
            informbar.Text = "Создание упаковки";
        }

        private void EnableandVisible( int statsAvailable = 2)
        {

            informbar.ForeColor = Color.Black;
            aButt1.Enabled = true;
            informbar.Visible = true;
            textIn.Enabled = true;
            textIn2.Enabled = true;
            stats1.Visible = true;
            stats2.Visible = true;
            RemoveAllButton.Enabled = true;
            RemoveFlowerButton.Enabled = true;
            aButt1.Text = "Создать";
        }

        private void UnableAndUnvisible (int statsAvailable = 2)
        {
            textIn.Text = "";
            textIn2.Text = "";
            textIn3.Text = "";
            textIn4.Text = "";
            stats1.Visible = false;
            stats2.Visible = false;
            stats3.Visible = false;
            stats4.Visible = false;
            textIn.Enabled = false;
            textIn2.Enabled = false;
            textIn3.Enabled = false;
            textIn4.Enabled = false;
            typeOfPlant1.Enabled = false;
            typeOfPlant2.Enabled = false;
            typeOfFlow1.Visible = false;
            typeOfFlow2.Visible = false;
            typeOfFlow3.Visible = false;
            typeOfFlow1.Enabled = false;
            typeOfFlow2.Enabled = false;
            typeOfFlow3.Enabled = false;
            typeOfFlow4.Enabled = false;
            typeOfFlow4.Visible = false;
            typeOfPlantlogo.Enabled = false;
            aButt1.Text = "Собрать";
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void materialpricelogo_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void packmateriallogo_Click(object sender, EventArgs e)
        {

        }

        private void flowersBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (flowersBox.SelectedIndex >= 0)
            {
                flowname.Visible = true;
                flowprice.Visible = true;
                int indexFlower = flowersBox.SelectedIndex;
                flowname.Text = flowersList[indexFlower].name;
                flowprice.Text = Convert.ToString(flowersList[indexFlower].price);

            }
        }

        private void typeOfPlant1_CheckedChanged(object sender, EventArgs e)
        {
            stats3.Visible = true;
            stats3.Text = "Страна";
            textIn3.Enabled = true;
            typeOfFlow1.Visible = true;
            typeOfFlow2.Visible = true;
            typeOfFlow3.Visible = true;
            typeOfFlow1.Enabled = true;
            typeOfFlow2.Enabled = true;
            typeOfFlow3.Enabled = true;
            typeOfFlow4.Enabled = true;
            typeOfFlow4.Visible = true;
            typeOfFlow1.Text = "Роза";
            typeOfFlow2.Text = "Тюльпан";
            typeOfFlow3.Text = "Пион";
            typeOfFlow4.Text = "Произвольно";
            typeOf.Text = "Тип цветка";
        }

        private void typeOfPlant2_CheckedChanged(object sender, EventArgs e)
        {
            stats3.Visible = true;
            stats3.Text = "Оттенок зелёного";
            textIn3.Enabled = true;
            typeOfFlow1.Enabled = true;
            typeOfFlow2.Enabled = true;
            typeOfFlow3.Enabled = true;
            typeOfFlow1.Visible = true;
            typeOfFlow2.Visible = true;
            typeOfFlow3.Visible = true;
            typeOfFlow4.Visible = false;
            typeOfFlow1.Text = "Папоротник";
            typeOfFlow2.Text = "Берграс";
            typeOfFlow3.Text = "Произвольно";
            typeOf.Text = "Тип зелени";
        }

        private void typeOfPlant3_CheckedChanged(object sender, EventArgs e)
        {
            stats3.Visible = false;
            textIn3.Enabled = false;
            typeOfFlow1.Enabled = false;
            typeOfFlow2.Enabled = false;
            typeOfFlow3.Enabled = false;
            typeOfFlow4.Enabled = false;
            typeOfFlow1.Visible = false;
            typeOfFlow2.Visible = false;
            typeOfFlow3.Visible = false;
            typeOfFlow4.Visible = false;
            typeOf.Text = "";
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void typeOfFlow1_CheckedChanged(object sender, EventArgs e)
        {
            if (typeOfFlow1.Text == "Роза")
            {
                stats4.Visible = true;
                textIn4.Enabled = true;
                stats4.Text = "Цвет";
            }

            if (typeOfFlow1.Text == "Папоротник")
            {
                textIn4.Enabled = true;
                stats4.Visible = true;
                stats4.Text = "Количество листов";
            }
        }

        private void typeOfFlow2_CheckedChanged(object sender, EventArgs e)
        {
            if (typeOfFlow2.Text == "Тюльпан")
            {
                textIn4.Enabled = true;
                stats4.Visible = true;
                stats4.Text = "Количество лепестков";
            }

            if (typeOfFlow2.Text == "Берграс")
            {
                textIn4.Enabled = true;
                stats4.Visible = true;
                stats4.Text = "Длина стебеля";
            }
        }

        private void typeOfFlow3_CheckedChanged(object sender, EventArgs e)
        {
            if (typeOfFlow3.Text == "Пион")
            {
                textIn4.Enabled = true;
                stats4.Visible = true;
                stats4.Text = "Размер бутона";
            }

            if (typeOfFlow3.Text == "Произвольно")
            {
                textIn4.Enabled = false;
                stats4.Visible = false;
                stats4.Text = "";
            }
        }

        private void typeOfFlow4_CheckedChanged(object sender, EventArgs e)
        {
            textIn4.Enabled = false;
            stats4.Visible = false;
            stats4.Text = "";
        }

        private void RemoveFlowerButton_Click(object sender, EventArgs e)
        {
            flowersList.RemoveAt(flowersBox.SelectedIndex);
            flowersBox.Items.RemoveAt(flowersBox.SelectedIndex);
            if (flowersList.Count == 0)
                RemoveFlowerButton.Enabled = false;
        }

        private void RemoveAllButton_Click(object sender, EventArgs e)
        {
            flowersList.Clear();
            flowersBox.Items.Clear();
            RemoveAllButton.Enabled = false;
            RemoveFlowerButton.Enabled = false;
        }
    }
}
