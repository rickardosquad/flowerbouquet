﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bouquet;

namespace TXI_Laba__5
{
        public class WinBouquetMaker
        {
            public Bouquet bouquet = new Bouquet();
            public Package package = new Package();
            public Flowers flowers = new Flowers();
            public int typeOfPlant;

        public void AddFlower()
            { 

            }
    

            public Flowers SetFlower(string name, int price)
            {
                flowers.SetName(name);
                flowers.SetPrice(price);
                return flowers;
            }

            public CutFlowers SetCutFlowersParams(string name, int price, string country)
            {
                CutFlowers cutFlowers = new CutFlowers();
                cutFlowers.SetName(name);
                cutFlowers.SetPrice(price);
                cutFlowers.SetCountry(country);
                return cutFlowers;
            }

            public Greens SetGreensParams(string name, int price, string toneOfGreen)
            {
                Greens greens = new Greens();
                greens.SetName(name);
                greens.SetPrice(price);
                greens.SetToneOfGreen(toneOfGreen);
                return greens;
            }
            
        public Rose SetRose(CutFlowers cutflower, string color)
        {
            Rose rose = new Rose();
            rose.name = cutflower.name;
            rose.price = cutflower.price;
            rose.country = cutflower.country;
            rose.color = color;
            return rose;
        }

        public Charmomile SetCharmomile(CutFlowers cutflower, int petals)
        {
            Charmomile charmomile = new Charmomile();
            charmomile.name = cutflower.name;
            charmomile.price = cutflower.price;
            charmomile.country = cutflower.country;
            charmomile.countOfPetals = petals;
            return charmomile;
        }

        public Peony SetPeony(CutFlowers cutflower, int sizeBurgeon)
        {
            Peony peony = new Peony();
            peony.name = cutflower.name;
            peony.price = cutflower.price;
            peony.country = cutflower.country;
            peony.sizeBurgeon = sizeBurgeon;
            return peony;
        }

        public Fern SetFern (Greens greens, int leaves)
        {
            Fern fern = new Fern();
            fern.name = greens.name;
            fern.price = greens.price;
            fern.toneOfGreen = greens.toneOfGreen;
            fern.SetNumberOfLeaf(leaves);
            return fern;
        }

        public Bergras SetBergras(Greens greens, int length)
        {
            Bergras bergras = new Bergras();
            bergras.name = greens.name;
            bergras.price = greens.price;
            bergras.toneOfGreen = greens.toneOfGreen;
            bergras.SetLength(length);
            return bergras;
        }

        public Package MakePackage(string material, int price)
            {
                package.SetMaterial(material);
                package.SetCostOfMaterial(price);
                return package;
                
            }

            public void ReadFlower(int i)
            {
                Flowers flowers = bouquet.GetFlowers(i);
                Console.WriteLine("Имя цветка: " + flowers.name + ", Цена равна: " + flowers.price);
            }

            public int CountPriceOfBouquet(List<Flowers> flowersList, Package package)
            {
                int priceOfBouquet = 0;
                foreach (Flowers c in flowersList)
                    priceOfBouquet += c.price;
                    priceOfBouquet += package.costOfMaterial;
            return priceOfBouquet;
            }
        }
}
